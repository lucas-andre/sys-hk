# Hacker news sys

Small client server system that consumes news from the hacker news API and displays it to the client in chronological order.

## Build & Run

Run the application with the following command with sudo

```
docker-compose up --build
```
and then go to [localhost:4200](http://localhost:4200)

## Authors

* **Lucas Henry** - *All task* - [Linkedin](https://www.linkedin.com/in/lucas-henryd/)

