import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  private baseUrl: string = environment.BASE_URL;
  // private endPointUrl = this.baseUrl + '/news';

  constructor(private http: HttpClient) { }

  getNews() {
    const url = `${this.baseUrl}/news`;
    return this.http.get(url, { withCredentials: true });
  }

  deleteNew(id) {
    const url = `${this.baseUrl}/news/${id}`;
    return this.http.delete(url, {  withCredentials: true });
  }

}
