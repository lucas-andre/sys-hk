import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req, next) {
    const token = localStorage.getItem('token');
    if (!token) { return next.handle(req); }
    const tokenizedReq = req.clone({
      setHeaders: {
        'X-Token': token
      },
    });
    return next.handle(tokenizedReq);
  }
}
