import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpSessionInterceptor } from './http-session.interceptor';
import { TokenInterceptor } from './token.interceptor';

export const httpInterceptorProvides = [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: HttpSessionInterceptor, multi: true},
];

