import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoCacheHeadersInterceptor implements HttpInterceptor {intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authReq = req.clone({
      // Inspiracion: https://medium.com/better-programming/how-to-prevent-http-request-caching-with-angular-httpclient-e82abf8b157d
      // Prevent caching in IE, in particular IE11.
      // See: https://support.microsoft.com/en-us/help/234067/how-to-prevent-caching-in-internet-explorer
      // Mirar igual: https://stackoverflow.com/questions/49547/how-do-we-control-web-page-caching-across-all-browsers?rq=1
      // https://stackoverflow.com/questions/53232382/how-to-disable-caching-with-httpclient-get-in-angular-6
      // https://stackoverflow.com/questions/54024779/how-to-prevent-browser-cache-on-angular-7-site
      setHeaders: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: '0'
      }
    });
    return next.handle(authReq);
  }
}

