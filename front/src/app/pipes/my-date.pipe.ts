import { Pipe, PipeTransform, Inject, LOCALE_ID } from '@angular/core';

import { DatePipe } from '@angular/common';

@Pipe({
    name: 'myDate',
})
export class MyDate implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string,  private datePipe: DatePipe) { }

    transform(value: string) {

        const now = new Date();
        const createDate = new Date(value);
        const midnigth = new Date();
        midnigth.setHours(0, 0, 0, 0);

        const midnigthHoursPassed = Math.floor(hourDiff(now, midnigth));
        const diffHour = Math.floor(hourDiff(now, createDate));

        if (diffHour < midnigthHoursPassed) {
            const myDate = this.datePipe.transform(value, 'h:mm a', this.locale);
            return myDate;
        }
        if (diffHour >=  midnigthHoursPassed && diffHour <= 48) {
            return 'Yesterday';
        }
        if (diffHour > 48) {
            const myDate = this.datePipe.transform(value, 'MMM d', this.locale);
            return myDate;
        }

    }
}

function hourDiff(nowDate, createDate) {

    const diffInMs = Date.parse(nowDate) - Date.parse(createDate);
    return diffInMs / 1000 / 60 / 60;
}


