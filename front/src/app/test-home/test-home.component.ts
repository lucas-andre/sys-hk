import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState, selectNewsState } from '../store/app.states';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { GetNews, DeleteNew } from '../store/app.actions';

@Component({
  selector: 'app-test-home',
  templateUrl: './test-home.component.html',
  styleUrls: ['./test-home.component.css']
})
export class TestHomeComponent implements OnInit {

  constructor( private store: Store<AppState>) {
      this.state$ = this.store.select(selectNewsState);
    }
  public isLoading: Observable<boolean>;
  public news: Observable<boolean>;
  private state$: Observable<any>;

  ngOnInit( ) {
    this.store.dispatch(new GetNews({}));
    this.isLoading = this.state$.pipe(
      map(news => news.loadingNews),
    );

    this.news = this.state$.pipe(
      map(news => news.news),
    );
  }

  // tslint:disable-next-line: variable-name
  remove(_new: any) {
    this.store.dispatch(new DeleteNew(_new));
    console.log('removing _new', _new);
  }

  goToUrl(clickedNew) {
    console.log('clicked new', clickedNew);
    if (clickedNew.url) {
      window.open(clickedNew.url, '_blank');
      return;
    }
    if (clickedNew.story_url) {
      window.open(clickedNew.story_url, '_blank');
      return;
    }
  }

}
