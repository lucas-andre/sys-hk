import * as news from './hk-news/news.reducers';
import { createFeatureSelector } from '@ngrx/store';

export interface AppState {
    feedSate: news.State;
}

export const reducers = {
    feed: news.reducer,
};

export const selectNewsState = createFeatureSelector<AppState>('feed');
