import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { AppComponent } from './app.component';
import { TestHomeComponent } from '../test-home/test-home.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NewsEffects } from './hk-news/news.effects';
import { extModules } from '../build-specifics/index';
import { reducers } from './app.states';
import { APP_ROUTES } from './app-routing.module';

import { MatDividerModule, MatListModule, MatButtonModule, MatIconModule } from '@angular/material';
import { MyDate } from '../pipes/my-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TestHomeComponent,
    MyDate,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    APP_ROUTES,
    CommonModule,
    NoopAnimationsModule,
    MatDividerModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    StoreModule.forRoot(
      reducers,
      {
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        }
      },
    ),
    extModules,
    EffectsModule.forRoot([NewsEffects]),
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
