
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { MainService } from '../../core/main.service';
import { AppActionTypes, GetNews, GetNewsSuccess, GetNewsFailure, DeleteNew, DeleteSuccess, DeleteFailure } from '../app.actions';

@Injectable()
export class NewsEffects {

  constructor(
    private actions: Actions,
    private mainService: MainService,
    private router: Router
  ) { }

  @Effect()
  GetNews: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.GET_NEWS),
    map((action: GetNews) => action.payload),
    switchMap(() => {
      return this.mainService.getNews().pipe(
        map((news: any) => {
          console.log('news', news);
          news.sort(((val1, val2) => {
            // tslint:disable-next-line: no-angle-bracket-type-assertion
            return <any> new Date(val2.created_at) - <any> new Date(val1.created_at);
          }));
          return new GetNewsSuccess({news});
        }),
        catchError((error) => {
          console.log(error);

          return of(new GetNewsFailure({ error }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  GetNewSuccess: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.GET_NEWS_SUCCESS),
    map((action: GetNewsSuccess) => action.payload),
    // tap((payload) => { })
  );

  @Effect({ dispatch: false})
  GetNewFailure: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.GET_NEWS_FAILURE),
    map((action: GetNewsFailure) => action.payload),
    // tap((payload) => { })
  );

  @Effect()
  DeleteNew: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.DELETE_NEW),
    map((action: DeleteNew) => action.payload),
    switchMap(payload => {
      return this.mainService.deleteNew(payload.id).pipe(
        map(status => new DeleteSuccess(status)),
        catchError(error => of(new DeleteFailure(error)))
      );
    }),
  );

  @Effect({ dispatch: false })
  DeleteSuccess: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.DELETE_SUCCESS),
    map((action: DeleteSuccess) => action.payload),
    // tap((payload) => { })
  );

  @Effect({ dispatch: false})
  DeleteFailure: Observable<any> = this.actions.pipe(
    ofType(AppActionTypes.DELETE_FAILURE),
    map((action: DeleteFailure) => action.payload),
    // tap((payload) => { })
  );

}
