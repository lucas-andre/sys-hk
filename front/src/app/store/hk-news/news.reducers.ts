import { All, AppActionTypes } from '../app.actions';
import { News } from 'src/app/core/interfaces/news.interface';

export interface State {
    news: News[] | null;
    errorMessage: string | null;
    loadingNews: boolean;
    lastUpdate: Date;
}

export const initialState: State = {
    news: null,
    errorMessage: null,
    loadingNews: false,
    lastUpdate: null
};

export function reducer(state = initialState, action: All): State {
    switch (action.type) {
        case AppActionTypes.GET_NEWS: {
            return {
                ...state,
                loadingNews: true,
            };
        }
        case AppActionTypes.GET_NEWS_SUCCESS: {
            return {
                ...state,
                news: action.payload.news,
                lastUpdate: new Date(),
            };
        }

        case AppActionTypes.GET_NEWS_FAILURE: {
            return {
                ...state,
                errorMessage: 'Get news fail',
            };
        }

        case AppActionTypes.DELETE_NEW: {
            return {
                ...state,
                lastUpdate: new Date(),
            };
        }

        case AppActionTypes.DELETE_SUCCESS: {
            return {
                ...state,
                news: state.news.filter((n: any) => n.id !== action.payload.baned_new),
                errorMessage: null,
            };
        }
        case AppActionTypes.DELETE_FAILURE: {
            return {
                ...state,
                errorMessage: 'Delete news fail',
            };
        }

        default: {
            return state;
        }
    }
}
