import { Action } from '@ngrx/store';

export enum AppActionTypes {
    GET_NEWS = '[App] Get News',
    GET_NEWS_SUCCESS = '[App] Get News Success',
    GET_NEWS_FAILURE = '[App] Get News Failure',
    DELETE_NEW = '[App] Delete New',
    DELETE_SUCCESS = '[App] Delete Success',
    DELETE_FAILURE = '[App] Delete Failure',
}

export class GetNews implements Action {

    readonly type = AppActionTypes.GET_NEWS;

    constructor(public payload: any) { }
}

export class GetNewsSuccess implements Action {

    readonly type = AppActionTypes.GET_NEWS_SUCCESS;

    constructor(public payload: any) { }
}

export class GetNewsFailure implements Action {

    readonly type = AppActionTypes.GET_NEWS_FAILURE;

    constructor(public payload: any) { }
}

export class DeleteNew implements Action {

    readonly type = AppActionTypes.DELETE_NEW;

    constructor(public payload: any) { }
}

export class DeleteSuccess implements Action {

    readonly type = AppActionTypes.DELETE_SUCCESS;

    constructor(public payload: any) { }
}

export class DeleteFailure implements Action {

    readonly type = AppActionTypes.DELETE_FAILURE;

    constructor(public payload: any) { }
}

export type All =
    | GetNews
    | GetNewsSuccess
    | GetNewsFailure
    | DeleteNew
    | DeleteSuccess
    | DeleteFailure;
