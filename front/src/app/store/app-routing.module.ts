import { Routes, RouterModule } from '@angular/router';
import { TestHomeComponent } from '../test-home/test-home.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'test-home',
        pathMatch: 'full',
    },
    {
        path: 'test-home',
        component: TestHomeComponent,
    },
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes);
