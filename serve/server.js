const connectDb = require('./src/db/connection');
const newsService = require("./src/services/news.service");
const app = require('./src/app');

const PORT = 8080;

const bootstrapApp = (cb) => {
  connectDb()
  .then( () => {
    console.log("MongoDb connected");
    newsService.getNews((err, news) => {
      newsService.saveNews(news).then(() => {
        cb(null, true);
      }).catch(err => {
        cb('Bootstrap fail', null);
      });
    })
  })
}

bootstrapApp((err, ok) => {
  if (err) return Error(err);
  if (ok) app.listen(PORT, () => console.log(`Listen in port ${PORT}`));
});

