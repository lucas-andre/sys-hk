const mongoose = require('mongoose');
const dbHandler = require('./db-handler');
const request = require('supertest');
const app = require('../app');
const newsController = require('../controllers/news.controllers');
const newModel = require('../schemas/new.model');
const sessionModel = require('../schemas/session.model');

beforeAll(async () => await dbHandler.connect());

beforeEach(() => {
    _new = new newModel(testNewData.normalNew);
    return _new.save()
})

beforeEach(() => {
    session = new sessionModel({ 
        key: {},
        baned_news: [{
            id: "5e1e844e5c44fe02e755c8e7",
            story_tile: "Digital Twins Are Reinventing Innovation",
            title: null,
            author: "vonnik",
            created_at: "2020-01-14T21:19:36.000Z",
            story_url: "https://sloanreview.mit.edu/article/how-digital-twins-are-reinventing-innovation/"
        }]
    });
    
    return session.save()
})

afterEach(async () => await dbHandler.clearDatabase());

afterAll(async () => await dbHandler.closeDatabase());

describe('Test root path', () => {
    test('It shoud response the GET method', () => {
        return request(app).get('/').then(response => {
            expect(response.statusCode).toBe(404)
        })
    });
})

describe('Test news path', () => {
    test('It shoud response the GET method', () => {
        return request(app).get('/news').send({}).then(response => {
            // console.log('response', response);
            expect(response.statusCode).toBe(200)
            expect(response.body[0]).toHaveProperty('id')
            expect(response.body[0]).toHaveProperty('title');
            expect(response.body[0]).toHaveProperty('author');
            expect(response.body[0]).toHaveProperty('created_at');
            expect(response.body[0]).toHaveProperty('story_url');
        })
    });
})


describe('Test news path', () => {
    test('It shoud response the DELETE method with empty body', () => {
        return request(app).delete('/news').send({}).then(response => {
            expect(response.statusCode).toBe(404)
        })
    });
})

const testNewData = {
    normalNew: 
        {
            id: "5e1e844e5c44fe02e755c8e7",
            story_tile: "Digital Twins Are Reinventing Innovation",
            title: null,
            author: "vonnik",
            created_at: "2020-01-14T21:19:36.000Z",
            story_url: "https://sloanreview.mit.edu/article/how-digital-twins-are-reinventing-innovation/"
        },

    
}

const sessionData = {

}
