var express = require('express');
var router = express.Router();

var NewsController = require('../controllers/news.controllers');

router.get('/news', NewsController.getNews);
router.delete('/news/:id', NewsController.deleteNew);

module.exports = router;