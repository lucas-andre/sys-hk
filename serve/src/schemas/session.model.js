const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema({
  key: { 
    type: Object,
    default: { }
  },
  baned_news: {
    type: Array,
    default: [],
  }
});

const Session = mongoose.model("Session", sessionSchema);

module.exports = Session;

