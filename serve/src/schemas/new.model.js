const mongoose = require("mongoose");

const newSchema = new mongoose.Schema({
  story_title: {
    type: String,
  },
   title: {
    type: String,
  },
  author: {
    type: String,
  },
  created_at: {
    type: Date,
  },
  story_url: {
      type: String,
  }
});

newSchema.pre('save', (next) => {
    this.wasNew = this.isNew;
    next();
    // if(this.isNew) { next() }
    // next('[Warning] New is ready in the database');
});

newSchema.post('save', () => {
    if(!this.wasNew) {
        
    }
});
const New = mongoose.model("New", newSchema);

module.exports = New;
