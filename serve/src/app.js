const express = require('express');
const cors = require('cors');
const app = express();
const newsRoutes = require("./routes/news.routes");
const session = require('express-session');
const cron = require("node-cron");
const fs = require("fs");

const newsService = require("./services/news.service");

app.use(cors({
  credentials: true,
  origin: true,
  exposedHeaders: ['X-Token'],
}));

app.use(session({
  secret: "secret-key",
  resave: false,
  saveUninitialized: true,
}));
cron.schedule("* 1 * * *", function() {
  newsService.getNews((err, news) => {
    console.log('getting news', news);
    newsService.saveNews(news).then(() => {
      console.log('updated news')
    }).catch(err => {
      throw new Error('Update news fail');
    });
  })
});

app.use('/', newsRoutes);

module.exports = app;