const Session = require("../schemas/session.model");

exports.findAndUpdate = async (id, session) => {
  if (!id) throw new Error('Missing id');
  if (!session) throw new Error('Missing session')
   await Session.findByIdAndUpdate(id, {key: session});
}
